import { Injector } from "app/ui/injectors/Injector";
import UltravioletHooks from "app/event/UltravioletHooks";

export default class RenderedVisualsInjector implements Injector {
    init(): PromiseOrNot<void> {
        // Show Ultraviolet-only visuals.
        document
            .querySelectorAll(".uv-show")
            .forEach((e) =>
                (e as HTMLElement).style.setProperty(
                    "display",
                    "unset",
                    "important"
                )
            );

        // Hide anti-Ultraviolet visuals
        document
            .querySelectorAll(".uv-hide")
            .forEach((e) => ((e as HTMLElement).style.display = "none"));

        UltravioletHooks.addHook("deinit", () => {
            // Hide Ultraviolet-only visuals.
            document
                .querySelectorAll(".uv-show")
                .forEach((e) =>
                    (e as HTMLElement).style.removeProperty("display")
                );

            // Show anti-Ultraviolet visuals
            document
                .querySelectorAll(".uv-hide")
                .forEach((e) =>
                    (e as HTMLElement).style.removeProperty("display")
                );
        });
    }
}
