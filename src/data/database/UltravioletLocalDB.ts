import UltravioletIDB from "app/data/database/UltravioletIDB";
import {
    APP_DATABASE_NAME,
    APP_DATABASE_VERSION,
} from "app/data/UltravioletConstants";
import UltravioletIDBObjectStore from "app/data/database/UltravioletIDBObjectStore";
import {
    CachedDependency,
    CacheTracker,
    LogItem,
    RecentPage,
    WatchedPage,
} from "app/data/database/UVDBObjectStoreDefinitions";
import Group from "app/mediawiki/core/Group";
import Log from "app/data/AppLog";
import UltravioletIDBUpgraders from "app/data/database/UltravioletIDBUpgraders";

export default class UltravioletLocalDB {
    public static i = new UltravioletLocalDB();

    // Object stores go below.
    cacheTracker: UltravioletIDBObjectStore<CacheTracker>;
    dependencyCache: UltravioletIDBObjectStore<CachedDependency>;
    groupCache: UltravioletIDBObjectStore<Group>;
    watchedPages: UltravioletIDBObjectStore<WatchedPage>;
    recentPages: UltravioletIDBObjectStore<RecentPage>;
    errorLog: UltravioletIDBObjectStore<LogItem>;
    combinedLog?: UltravioletIDBObjectStore<LogItem>;
    // Object stores go above.

    private _open: boolean;
    private idb: UltravioletIDB;

    public get open(): boolean {
        return this._open;
    }

    private constructor() {
        if (UltravioletLocalDB.i != null)
            throw new Error(
                "UltravioletLocalDB already exists! (as `UltravioletLocalDB.i`)"
            );

        this.idb = new UltravioletIDB(
            APP_DATABASE_NAME,
            APP_DATABASE_VERSION,
            UltravioletIDBUpgraders
        );
    }

    async connect(): Promise<IDBDatabase> {
        Log.trace("Connecting to Ultraviolet IDB...");
        const connect = await this.idb.connect();
        Log.trace("Connected to IDB.");

        // Handle _open
        this._open = true;
        connect.addEventListener("close", () => {
            this._open = false;
        });

        // Apply all database definitions
        this.cacheTracker = this.idb.store<CacheTracker>("cacheTracker");
        this.dependencyCache =
            this.idb.store<CachedDependency>("dependencyCache");
        this.groupCache = this.idb.store<Group>("groupCache");
        this.watchedPages = this.idb.store<WatchedPage>("watchedPages");
        this.recentPages = this.idb.store<RecentPage>("recentPages");

        this.errorLog = this.idb.store<LogItem>("errorLog");
        // TODO only on debug mode
        //this.combinedLog = this.idb.store<LogItem>("combinedLog");

        return connect;
    }
}
