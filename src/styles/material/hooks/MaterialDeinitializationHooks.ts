import UltravioletStore from "app/data/UltravioletStore";
import { MaterialStyleStorage } from "app/styles/material/data/MaterialStyleStorage";
import Log from "app/data/AppLog";

export default function (): void {
    const store: MaterialStyleStorage =
        UltravioletStore.styleStorage as MaterialStyleStorage;
    Log.info("Disconnecting MutationObserver...");
    store.mutationObserver.disconnect();
    Log.info("MutationObserver disconnected.");
    store.intervals.forEach((e) => clearInterval(e));
}
