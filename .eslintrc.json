{
    "parser": "@typescript-eslint/parser",
    "env": {
        "commonjs": true,
        "es6": true,
        "node": false,
        "browser": true
    },
    "extends": [
        "plugin:@typescript-eslint/recommended",
        "plugin:import/errors",
        "plugin:import/warnings",
        "plugin:import/typescript",
        "plugin:prettier/recommended"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2020,
        "ecmaFeatures": {
            "impliedStrict": true
        }
    },
    "rules": {
        // ====================================================================
        // TypeScript Rules
        // ====================================================================
        // Allow explicit `any`s.
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        // Ignore unused TSX parameters, since `h` will never be used normally.
        "@typescript-eslint/no-unused-vars": [
            "warn",
            {
                "varsIgnorePattern": "h"
            }
        ],
        // TypeScript handles unresolved import issues.
        "import/no-unresolved": "off",
        "import/no-named-as-default": "off",

        // ====================================================================
        // Semantics
        // ====================================================================
        // Prefer constants instead of `var` or `let` when not reassigned.
        "prefer-const": [
            "error",
            {
                "destructuring": "all"
            }
        ],
        // Prevent `this` in static usages. Use the class name instead for clarity.
        "no-restricted-syntax": [
            "error",
            {
                "selector": "MethodDefinition[static = true] ThisExpression",
                "message": "`this` is being used in a static context."
            }
        ],
        "prettier/prettier": ["error", { "endOfLine": "auto" }]
    },
    "settings": {
        "import/parsers": { "@typescript-eslint/parser": [".ts", ".tsx"] }
    }
}
